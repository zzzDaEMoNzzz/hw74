const express = require('express');
const fs = require('fs');

const router = express.Router();

const path = './messages/';

fs.access(path, err => {
  if (err && err.code === 'ENOENT') {
    fs.mkdir(path, () => {});
  }
});

router.get('/', (req, res) => {
  fs.readdir(path, (err, files) => {
    let filesList = [];

    files.forEach(file => filesList.push(`${path}${file}`));

    const firstFile = filesList.length - 5 >= 0 ? filesList.length - 5 : 0;
    const filesContents = filesList.slice(firstFile).map(filePath => {
      return JSON.parse(fs.readFileSync(filePath, 'utf8'));
    });

    res.send(filesContents);
  });
});

router.post('/', (req, res) => {
  const datetime = new Date().toJSON();
  fs.writeFileSync(`${path}${datetime}.txt`, JSON.stringify(req.body, null, 2));

  res.send({...req.body, datetime});
});

module.exports = router;